const byte outPin1 = 5;
const byte interruptPin1 = 2;
volatile byte fitpixBusy1 = LOW;
volatile byte daqBusy1 = LOW;

void setup() {
  Serial.begin(115200);
  pinMode(outPin1, OUTPUT);
  pinMode(interruptPin1, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin1), busyON1, CHANGE);
}

void loop() {
  digitalWrite(outPin1, fitpixBusy1 || daqBusy1);
}

void busyON1() {
  fitpixBusy1 = digitalRead(interruptPin1);
}

void serialEvent() {    
  while (Serial.available()) { //here wait for busy OFF signal on serial
    // get the new byte:
    char inChar = (char)Serial.read();
        
    if (inChar == 'l') {
      daqBusy1 = LOW;
    }
    else if (inChar == 'h') {
      daqBusy1 = HIGH;
    }
  }
}
